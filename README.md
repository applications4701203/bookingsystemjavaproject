# BookingSystemJavaProject

This is a Java Application made as part of the job application for JPMorgan.

## Getting started

This is a simple Java 17 Program featuring a booking system where ADMIN users can setup and view show details, and 
CUSTOMER users can check show and seat availability, book a seat in the selected show, and even cancel a previous booking, 
provided it is within the cancellation period.

Please see the comments in the src code for further details.

## Use Case (as provided):

1. Build a simple Java application for the use case to book a show. The program must take input from command line. 
2. The program would setup available seats per show, allow buyers to select 1 or more available seats and buy/cancel tickets. 
3. The application shall cater to the below 2 types of users & their requirements – (1) Admin and (2) Buyer

4. Admin – The users should be able to Setup and view the list of shows and seat allocations.

- Commands to be implemented for Admin :
- Setup the number of seats per show
- Display Show Number, Ticket#, Buyer Phone#, Seat Numbers allocated to the buyer

5. Buyer – The users should be able retrieve list of available seats for a show, select 1 or more seats , buy and cancel tickets.

- Commands to be implemented for Buyer :
- List all available seat numbers for a show. E,g A1, F4 etc
- Book a ticket. This must generate a unique ticket # and display
- Cancel a ticket

## Constraints

1. Assume max seats per row is 10 and max rows are 26. Example seat number A1,  H5 etc. The “Add” command for admin must ensure rows cannot be added beyond the upper limit of 26.
2. After booking, User can cancel the seats within a time window of 2 minutes (configurable).   Cancellation after that is not allowed.
3. Only one booking per phone# is allowed per show.

## Credentials

- For Admin:
Username: Admin, Password: AdminPassword

- For Customers: VALID so long as username and password are not empty.