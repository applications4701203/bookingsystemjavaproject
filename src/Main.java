import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

public class Main {

    // list of shows - static as this is the same for all methods in the main class
    static List<ShowBooking> listOfShows = new ArrayList<>();

    // List of Bookings per Show - static as this is the same for all methods in the main class
    // Key: Show title, Value: lists of bookings (map of ticket numbers, and ticket details)
    static Map<String, List<Booking>> showBookings = new HashMap<>();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        // Boolean variables to keep the application running!
        boolean systemRunning = true;
        boolean loggedIn = false;

//        UNCOMMENT for testing purpose only
//        listOfShows.add(new ShowBooking("Rewind", 26, 10, 2));
//        listOfShows.add(new ShowBooking("No Other Woman", 20, 10, 2));

        while (systemRunning) {
            System.out.println("Welcome to the Show Booking System!");
            System.out.println("1. Login");
            System.out.println("2. Exit");
            System.out.print("Choose an option (1 or 2): ");
            int option = scanner.nextInt();

            if (option == 1) {

                // Login Screen! Returns the type of User once validated.
                String credential = loginAndValidateCredentials();

                if (!credential.isEmpty()) {
                    // Login Successful
                    loggedIn = true;
                }

                while (loggedIn) {
                    switch (credential) {
                        case "ADMIN" -> {
                            System.out.println("Hello, Admin!");
                            loggedIn = adminScreen(loggedIn);
                        }
                        case "CUSTOMER" -> {
                            System.out.println("Hello, Customer!");
                            loggedIn = customerScreen(loggedIn);
                        }
                        default -> System.out.println("Invalid Credentials. Please try again!");
                    }
                }
            } else if (option == 2) {
                System.out.println("Thank you for using this Booking System! See you again!");
                systemRunning = false;
            } else {
                System.out.println("Please input a valid option!");
            }
        }

    }

    private static String loginAndValidateCredentials() {

        System.out.print("Username: ");
        String username = scanner.next();
        System.out.print("Password: ");
        String password = scanner.next();

        if (username.equals("Admin") && password.equals("AdminPassword")) {
            return "ADMIN";
        } else if (!username.isEmpty() && !password.isEmpty()) {
            return "CUSTOMER";
        } else {
            return "";
        }
    }

    private static boolean adminScreen(boolean loggedIn) {
        while (loggedIn) {

            // Admin Settings
            System.out.println("Available Options:");
            System.out.println("1. Setup Show");
            System.out.println("2. View Show Details");
            System.out.println("3. View Show Bookings");
            System.out.println("4. Exit");

            System.out.print("Choose an option (1-4): ");
            int option = scanner.nextInt();

            switch (option) {
                case 1 -> setupShow();
                case 2 -> displayListOfShows();
                case 3 -> displayShowBookings();
                case 4 -> {
                    System.out.println("Thank you, Admin. Goodbye!");
                    loggedIn = false;
                }
                default -> System.out.println("Invalid option. Please choose a valid option.");
            }
        }

        return false;
    }

    private static boolean customerScreen(boolean loggedIn) {
        while (loggedIn) {
            Optional<ShowBooking> selectedShow;

            System.out.println("Available Options:");
            System.out.println("1. View Show Availability");
            System.out.println("2. Book a Ticket");
            System.out.println("3. Cancel Booking");
            System.out.println("4. Exit");
            System.out.print("Choose an option (1-4): ");

            int option = scanner.nextInt();

            switch (option) {
                case 1 -> {

                    displayListOfShows();
                    selectedShow = selectShow();

                    if (selectedShow.isPresent()) viewShowAvailability(selectedShow.get());
                        else System.out.println("The show you selected is not available anymore!");

                }
                case 2 -> {
                    displayListOfShows();
                    selectedShow = selectShow();

                    if (selectedShow.isPresent()) bookTickets(selectedShow.get());
                        else System.out.println("The show you selected is not available anymore!");

                }
                case 3 -> {

                    displayListOfShows();
                    selectedShow = selectShow();

                    if (selectedShow.isPresent()) cancelTickets(selectedShow.get());
                        else System.out.println("The show you selected is not available anymore!");

                }
                case 4 -> {
                    System.out.println("Thank you, Customer. Goodbye!");
                    loggedIn = false;
                }
                default -> System.out.println("Invalid option. Please choose a valid option.");
            }
        }

        return false;
    }

    private static Optional<ShowBooking> selectShow() {

        System.out.print("Enter Show ID: ");
        int selectedShowNumber = scanner.nextInt();
        return listOfShows.stream()
                .filter(booking -> booking.getShowId() == selectedShowNumber).findFirst();
    }

    private static void cancelTickets(ShowBooking showBooking) {

        System.out.println("Cancel Booking:");
        System.out.println();
        System.out.print("Enter Ticket #: ");
        String ticketNum = scanner.next();
        System.out.print("Enter Phone #: ");
        String phoneNum = scanner.next();

        List<Booking> listOfBookings = showBookings.get(showBooking.getShowTitle());

        boolean phoneNumberExisting = listOfBookings.stream()
                .anyMatch(booking -> booking.getTicketBookings().containsKey(phoneNum));

        if (phoneNumberExisting) {

            BookingDetails bookingDetails = listOfBookings.stream().map(booking -> booking.getTicketBookings().get(phoneNum)).findFirst().orElse(null);

            if (bookingDetails != null) {

                System.out.println("bookingDetails: " + bookingDetails.getTicketNumber());

                if (isCancellationAllowed(bookingDetails, showBooking.getCancellationWindow())) {
                    System.out.println("Cancelling...");

                    listOfBookings.forEach(booking -> booking.getTicketBookings().remove(phoneNum));

                    System.out.println("Ticket Num: " + ticketNum + " is now cancelled!");
                } else {
                    System.out.println("Cancellation period has elapsed already!");
                }
            }

        } else {
            System.out.println("No booking is found for this phone number!");
        }

    }

    private static boolean isCancellationAllowed(BookingDetails bookingDetails, int cancellationWindow) {

        Duration duration = Duration.between(bookingDetails.getBookingDateTime(), LocalDateTime.now());
        long minutesElapsed = duration.toMinutes();

        return minutesElapsed <= cancellationWindow;
    }

    private static void viewShowAvailability(ShowBooking selectedShow) {
        System.out.println(selectedShow.viewShowDetails());

        // Only the unbooked seats will be displayed here!
        System.out.println("Available Seats: " + selectedShow.getListOfSeats());
    }

    private static void displayListOfShows() {
        if(listOfShows.isEmpty()) {
            System.out.println("There are no shows available!");
        } else {
            System.out.println("These are the shows available: ");

            for (ShowBooking sb : listOfShows) {
                System.out.println("ShowId: " + sb.getShowId() + " | " + sb.getShowTitle() + " | " + sb.getListOfSeats());
            }
        }
    }

    private static void displayShowBookings() {
        if(showBookings.isEmpty()) {
            System.out.println("There are no bookings registered!");
        } else {
            System.out.println("These are the bookings per show: ");

            for (Map.Entry<String, List<Booking>> entry : showBookings.entrySet()) {
                System.out.println("Show Title: " + entry.getKey());

                System.out.println("Bookings: ");
                for(Booking book: entry.getValue()) {
                    for (Map.Entry<String, BookingDetails> booking : book.getTicketBookings().entrySet()) {
                        System.out.println("Ticket Number: " + booking.getValue().getTicketNumber());
                        System.out.println("Phone Number: " + booking.getValue().getPhoneNumber());
                        System.out.println("List of Seats: " + booking.getValue().getSeatNumbers());
                    }
                }
            }
        }
    }

    private static void setupShow() {
        System.out.println("Setup Show:");
        System.out.print("1. Enter Show Title: ");
        String showTitle = scanner.next();

        System.out.print("2. Enter Number of Rows (1-26): ");
        int numOfRows = validateNumOfRows();

        System.out.print("3. Enter Number of Seats Per Row (1-10): ");
        int numOfSeatsPerRow = validateNumOfSeatsPerRow();

        System.out.print("4. Enter Number of Minutes for Cancellation (Default - 2 minutes): ");
        int cancellationWindow = validateCancellationWindow();

        System.out.println("Setting Up Show!");
        // Process of setting up show is in the constructor of ShowBooking class.
        listOfShows.add(new ShowBooking(showTitle, numOfRows, numOfSeatsPerRow, cancellationWindow));
    }

    private static int validateNumOfRows() {
        int numOfRows = scanner.nextInt();

        // To ensure rows cannot be added beyond the upper limit of 26.
        while (numOfRows < 1 || numOfRows > 26) {
            System.out.print("2. Enter Number of Rows (1-26): ");
            numOfRows = scanner.nextInt();
        }
        return numOfRows;
    }

    private static int validateNumOfSeatsPerRow() {
        int numOfSeatsPerRow = scanner.nextInt();

        // To ensure number of seats per row cannot be beyond the upper limit of 10.
        while (numOfSeatsPerRow < 1 || numOfSeatsPerRow > 10) {
            System.out.print("3. Enter Number of Seats Per Row (1-10): ");
            numOfSeatsPerRow = scanner.nextInt();
        }
        return numOfSeatsPerRow;
    }

    private static int validateCancellationWindow() {
        int cancellationWindow = scanner.nextInt();

        while (cancellationWindow < 1) {
            System.out.print("4. Enter Number of Minutes for Cancellation (Default - 2 minutes): ");
            cancellationWindow = scanner.nextInt();
        }
        return cancellationWindow;
    }

    private static void bookTickets(ShowBooking showBooking) {

        System.out.println("Available Seats: " + showBooking.getListOfSeats());

        System.out.println("Enter Seat Numbers (comma separated):");
        String seatNumber = scanner.next();
        System.out.println("Enter Phone Number:");
        String phoneNumber = scanner.next();

        // Check if the show has already a list of booking:
        // if there is -- get the bookings and add a new booking to the list of bookings
        // if none -- create a new key-value for the selected show, create a new list of bookings, and add the key-value to showBooking
        if (showBookings.containsKey(showBooking.getShowTitle())) {
            List<Booking> listOfBookings = showBookings.get(showBooking.getShowTitle());

            // REQUIREMENT: Only one booking per phone number is allowed per show.
            // Check if there is a booking for the phone number!
            boolean phoneNumberExisting = listOfBookings.stream()
                    .anyMatch(booking -> booking.getTicketBookings().containsKey(phoneNumber));

            if (!phoneNumberExisting) {

                // Comma Separated Seat Numbers
                List<String> listOfSeatNumbers = new ArrayList<>(Arrays.asList(seatNumber.split(",")));
                List<String> seatBooked = new ArrayList<>();

                for (String seat: listOfSeatNumbers) {
                    if (showBooking.getListOfSeats().stream().anyMatch(seats -> seats.equalsIgnoreCase(seat))) {
                        seatBooked.add(seat);
                        showBooking.getListOfSeats().remove(seat);
                    } else {
                        System.out.println("Seat " + seat +  " not booked as it is already taken!");
                    }
                }

                Booking newBook = new Booking(showBooking.getShowId(), phoneNumber, seatBooked);
                listOfBookings.add(newBook);
                System.out.println("Success! Your booking has been done.");
            } else {
                System.out.println("Phone Number has booking already!");
            }
        } else {
            List<Booking> listOfBookings = new ArrayList<>();

            List<String> listOfSeatNumbers = new ArrayList<>(Arrays.asList(seatNumber.split(",")));
            Booking newBook = new Booking(showBooking.getShowId(), phoneNumber, listOfSeatNumbers);
            listOfBookings.add(newBook);
            showBookings.put(showBooking.getShowTitle(), listOfBookings);
            showBooking.getListOfSeats().removeAll(listOfSeatNumbers);
            System.out.println("Success! Your booking has been done.");
        }
    }
}