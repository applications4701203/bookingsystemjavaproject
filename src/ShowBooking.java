import java.util.ArrayList;
import java.util.List;

class ShowBooking {

    private static int nextId = 1;

    private int showId;
    private String showTitle;
    private int numOfRows;
    private int numOfSeatsPerRow;
    private int cancellationWindow;

    private List<String> listOfSeats;

    public ShowBooking(String showTitle, int numOfRows, int numOfSeatsPerRow, int cancellationWindow) {

        // Process of setting up show

        this.showId = nextId++;
        this.showTitle = showTitle;
        this.numOfRows = numOfRows;
        this.numOfSeatsPerRow = numOfRows;

        // Default cancellation window - 2 minutes
        this.cancellationWindow = cancellationWindow == 0 ? cancellationWindow : 2 ;

        // Seat ID Generator
        List<String> seatNumbers = new ArrayList<>();
        for (int i = 0; i < numOfRows; i++) {
            for (int j = 0; j < numOfSeatsPerRow ; j++) {
                char letter = (char) ('A' + i);
                int number = j + 1;
                seatNumbers.add(Character.toString(letter) + number);
            }
        }

        this.listOfSeats = seatNumbers;

        System.out.println(showTitle + " is now available with " + (numOfRows + numOfSeatsPerRow) + " seats available.");
    }

    public String viewShowDetails() {
        return showId + " | " + showTitle + " | " + (numOfRows + numOfSeatsPerRow) + " seats available.";
    }

    public List<String> viewSeatAvailable() {
        return listOfSeats;
    }

    public int getShowId() {
        return showId;
    }

    public void setShowId(int showId) {
        this.showId = showId;
    }

    public String getShowTitle() {
        return showTitle;
    }

    public void setShowTitle(String showTitle) {
        this.showTitle = showTitle;
    }

    public int getNumOfRows() {
        return numOfRows;
    }

    public void setNumOfRows(int numOfRows) {
        this.numOfRows = numOfRows;
    }

    public int getNumOfSeatsPerRow() {
        return numOfSeatsPerRow;
    }

    public void setNumOfSeatsPerRow(int numOfSeatsPerRow) {
        this.numOfSeatsPerRow = numOfSeatsPerRow;
    }

    public int getCancellationWindow() {
        return cancellationWindow;
    }

    public void setCancellationWindow(int cancellationWindow) {
        this.cancellationWindow = cancellationWindow;
    }

    public List<String> getListOfSeats() {
        return listOfSeats;
    }

    public void setListOfSeats(List<String> listOfSeats) {
        this.listOfSeats = listOfSeats;
    }
}