import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

class Booking {
    private Map<String, BookingDetails> ticketBookings;
    public Booking(int showId, String phoneNumber, List<String> listOfSeatNumbers) {

        Map<String, BookingDetails> ticketBookings = new HashMap<>();
        String ticketNumber = generateTicketNumber();

        BookingDetails bookingDetails = new BookingDetails();
        bookingDetails.setTicketNumber(ticketNumber);
        bookingDetails.setShowId(showId);
        bookingDetails.setPhoneNumber(phoneNumber);

        bookingDetails.setSeatNumbers(listOfSeatNumbers);
        bookingDetails.setBookingDateTime(LocalDateTime.now());
        ticketBookings.put(phoneNumber, bookingDetails);

        System.out.println("Your ticket number is " + ticketNumber + "!");

        this.ticketBookings = ticketBookings;
    }

    public Map<String, BookingDetails> getTicketBookings() {
        return ticketBookings;
    }

    public void setTicketBookings(Map<String, BookingDetails> ticketBookings) {
        this.ticketBookings = ticketBookings;
    }

    private String generateTicketNumber() {
        long timestamp = System.currentTimeMillis();
        int random = new Random().nextInt(90000) + 10000;

        return String.valueOf(((timestamp % 100000) * 100 + random));
    }
}
